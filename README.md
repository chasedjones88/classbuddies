#ClassBuddies#

A study-group project for Android by some *class buddies.*

##Get the Source Code##

This project was made and built in Android Studio. It is also recommended to use this program to download the app via the "checkout from git version control" option on the main screen of the program, or to create a new project and clone the repository with the command `git clone https://thatbaby@bitbucket.org/thatbaby/classbuddies.git` in the Android terminal.

##Installation##

To install the program, navigate to the *Downloads* tab on the left and download the .apk. Simply run the .apk on an android device to install.

##Documents##

|Essential technical documents|
|-----------------------------|
|[System Requirement Specifications](https://drive.google.com/open?id=1rz-40QmW5ACctQo2gZJ13EII6LTMC7nncVqQpiNf1IE)|



##Crew##
The Class Buddies are:

|Buddy				| How to reach them			|
|-------------------|---------------------------|
|Aaron Cheung 		| aaron97c@gmail.com		|
|Charmaine Ching 	| cmchingrk@gmail.com		|
|Chase Jones		| chasedjones88@gmail.com	|
|Dennis Hon 		| dennishon3@gmail.com		|
|Jeffrey Porovich 	| jporovich@gmail.com		|
|Nicole Barakat 	| nicbarakat@gmail.com		|
|Robert Roati 		| robertroati@gmail.com		|
